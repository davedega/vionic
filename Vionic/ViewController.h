//
//  ViewController.h
//  Vionic
//
//  Created by Dave Delgado on 04/09/13.
//  Copyright (c) 2013 David Delgado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableVideos;

-(void)hazPeticion;
@end
