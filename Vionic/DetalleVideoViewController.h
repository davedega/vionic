//
//  DetalleVideoViewController.h
//  Vionic
//
//  Created by Dave Delgado on 05/09/13.
//  Copyright (c) 2013 David Delgado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetalleVideoViewController : UIViewController{
    IBOutlet UIWebView * webView;
    NSString *urlVideo;
}
@property (nonatomic,retain) NSString *urlVideo;

@end
