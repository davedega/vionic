//
//  ViewController.m
//  Vionic
//
//  Created by Dave Delgado on 04/09/13.
//  Copyright (c) 2013 David Delgado. All rights reserved.
//

#import "ViewController.h"
#import "VideoCell.h"
#import "Video.h"
#import "DetalleVideoViewController.h"

@interface ViewController (){
    NSArray *fotos;
    NSArray *urls;
    NSArray *titles;
    int seleccion;
}

@end

@implementation ViewController
@synthesize tableVideos;
- (void)viewDidLoad
{
    [super viewDidLoad];
    tableVideos.delegate=self;
    tableVideos.dataSource=self;

    [self hazPeticion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)hazPeticion{
    
    NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://gdata.youtube.com/feeds/api/videos?q='youtube'&format=5&max-results=10&v=2&alt=jsonc"]];
    
    NSError *jsonError;
    
    id jsonDictionaryOrArray = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONWritingPrettyPrinted error:&jsonError];
    fotos = [jsonDictionaryOrArray valueForKeyPath:@"data.items.thumbnail.sqDefault"];
    urls = [jsonDictionaryOrArray valueForKeyPath:@"data.items.player.default"];
    titles = [jsonDictionaryOrArray valueForKeyPath:@"data.items.title"];    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [titles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cellForRowAtIndexPath %@",[titles objectAtIndex:indexPath.row]);
    
    static NSString *CellIdentifier = @"videoCell";
    VideoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];    
    NSURL *url = [NSURL URLWithString:[fotos objectAtIndex:[indexPath row]]];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    UIImage *tmpImage = [[UIImage alloc] initWithData:data];
    cell.fotoCell.image = tmpImage;
    cell.tituloCell.text = [titles objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    seleccion=indexPath.row;
    NSLog(@"selected: %i",indexPath.row);
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DetalleVideoViewController *dVc = [segue destinationViewController];
    dVc.urlVideo=[urls objectAtIndex:seleccion];
}

@end
