//
//  DetalleVideoViewController.m
//  Vionic
//
//  Created by Dave Delgado on 05/09/13.
//  Copyright (c) 2013 David Delgado. All rights reserved.
//

#import "DetalleVideoViewController.h"

@interface DetalleVideoViewController ()

@end

@implementation DetalleVideoViewController
@synthesize urlVideo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"url: %@",urlVideo);
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlVideo]] ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
   

@end
