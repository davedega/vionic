//
//  main.m
//  Vionic
//
//  Created by Dave Delgado on 04/09/13.
//  Copyright (c) 2013 David Delgado. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
