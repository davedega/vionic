//
//  Video.h
//  Vionic
//
//  Created by Dave Delgado on 04/09/13.
//  Copyright (c) 2013 David Delgado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Video : NSObject{
	
    NSString *vUrlVideo;
    NSString *vUrlFoto;
    NSString *vTitle;

}
@property (nonatomic, copy)		NSString *vUrlVideo;
@property (nonatomic, copy)		NSString *vUrlFoto;
@property (nonatomic, copy)		NSString *vTitle;

-(id) initWithFoto:(NSString*) urlDeLaFoto andInitWithurlVideo:(NSString*) urlDelVideo andTitle:(NSString*)title;


@end
