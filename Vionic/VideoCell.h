//
//  VideoCell.h
//  Vionic
//
//  Created by Dave Delgado on 04/09/13.
//  Copyright (c) 2013 David Delgado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *fotoCell;
@property (strong, nonatomic) IBOutlet UILabel *tituloCell;

@end
