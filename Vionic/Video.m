//
//  Video.m
//  Vionic
//
//  Created by Dave Delgado on 04/09/13.
//  Copyright (c) 2013 David Delgado. All rights reserved.
//

#import "Video.h"

@implementation Video

-(id) initWithFoto:(NSString*) urlDeLaFoto andInitWithurlVideo:(NSString*) urlDelVideo andTitle:(NSString *)elTitle{
    if(self = [super init]){
        vUrlVideo=urlDelVideo;
        vUrlFoto=urlDeLaFoto;
        vTitle=elTitle;
    }
    return self;
}

@end
